package reson

import (
	"bytes"
	"encoding/binary"
	"io"
)

type zstring [256]byte

func (s zstring) String() string {
	idx := bytes.IndexByte(s[:], byte(0))
	if idx == -1 {
		return string(s[:])
	}
	return string(s[0:idx])
}

type StorageMode uint8

const (
	Stopped StorageMode = iota
	Recording
	Playing
	Deleting
	Unknown
)

//go:generate stringer -type=StorageMode

// 7kCenter Storage Status Information, type 7052
type StorageStatusHeader struct {
	Position     uint32      `json:"position"`
	Disk_free    uint8       `json:"disk_free"`
	Mode         StorageMode `json:"mode"`
	File_records uint32      `json:"file_records"`
	File_size    uint64      `json:"file_size"`
	First        Timestamp   `json:"first"`
	Last         Timestamp   `json:"last"`
	Duration     uint32      `json:"duration"`
	Directory    zstring     `json:"directory"`
	Filename     zstring     `json:"filename"`
	_            [8]uint32
}

type PrefixedArray struct {
	n    uint32
	Vals []uint32 `json:"vals"`
}

func (p *PrefixedArray) readInto(rdr io.Reader) error {
	err := binary.Read(rdr, ByteOrder, &p.n)
	if err != nil {
		return err
	}
	p.Vals = make([]uint32, p.n)
	return binary.Read(rdr, ByteOrder, p.Vals)
}

type StorageStatus struct {
	Hdr             StorageStatusHeader
	Thresholds      PrefixedArray
	IncludedRecords PrefixedArray
	ExcludedRecords PrefixedArray
	IncludedDevices PrefixedArray
	ExcludedDevices PrefixedArray
}

func init() {
	drRegistry[uint32(7052)] = func() DataRecord { return &StorageStatus{} }
}

// Unpack implements the reson.DataRecord interface
func (s *StorageStatus) Unpack(rdr io.Reader) error {
	err := binary.Read(rdr, ByteOrder, &s.Hdr)
	if err != nil {
		return err
	}
	(&s.Thresholds).readInto(rdr)
	(&s.IncludedRecords).readInto(rdr)
	(&s.ExcludedRecords).readInto(rdr)
	(&s.IncludedDevices).readInto(rdr)
	return (&s.ExcludedDevices).readInto(rdr)
}

// ReadStorageStatus reads a StorageStatus packet from a data source.
func ReadStorageStatus(rdr io.Reader) (*StorageStatus, error) {
	s := &StorageStatus{}
	err := s.Unpack(rdr)
	return s, err
}
