// Package reson implements a subset of the RESON 7k sonar network API for
// the COVIS project.
package reson

import (
	"bytes"
	"encoding/binary"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"log"
	"time"
)

// Package logging function
var LogFunc func(string, ...interface{})

// Byte order for all binary messages
var ByteOrder binary.ByteOrder = binary.LittleEndian

// Record type codes for Data Record Frames.
const (
	RTSettings      uint32 = 7000
	RTConfiguration uint32 = 7001
	RTRawiq         uint32 = 7038
	RTSysEvents     uint32 = 7050
	RTEventMsg      uint32 = 7051
	RTStorageStatus uint32 = 7052
	RTRemoteCtl     uint32 = 7500
	RTAck           uint32 = 7501
	RTNak           uint32 = 7502
)

// TCP port for 7k-Center server
const S7kPort = 7000

// TCP port for RCM server
const RCMPort = 50000

// 7k data record timestamp
type Timestamp struct {
	Year    uint16
	Day     uint16
	Seconds float32
	Hours   int8
	Minutes int8
}

// NewTimestamp returns a new data timestamp from the supplied time.Time
// value. Due to the limited precision of Timestamp.Seconds, the input
// value should be truncated to the nearest microsecond.
func NewTimestamp(t time.Time) Timestamp {
	return Timestamp{
		Year:    uint16(t.Year()),
		Day:     uint16(t.YearDay()),
		Seconds: float32(t.Second()) + float32(t.Nanosecond())*1.0e-9,
		Hours:   int8(t.Hour()),
		Minutes: int8(t.Minute()),
	}
}

// Convert a Timestamp to a time.Time value.
func (ts Timestamp) Time() time.Time {
	if ts.Year == 0 {
		return time.Time{}
	}
	base := time.Date(int(ts.Year), time.January, 1, 0, 0, 0, 0, time.UTC)
	t := base.Add(time.Hour * (24*time.Duration(ts.Day-1) + time.Duration(ts.Hours)))
	t = t.Add(time.Minute * time.Duration(ts.Minutes))
	return t.Add(time.Duration(ts.Seconds * 1.0e9)).Truncate(time.Microsecond)
}

// MarshalJSON implements the json.Marshal interface
func (ts Timestamp) MarshalJSON() ([]byte, error) {
	return json.Marshal(ts.Time())
}

// Value of the "sync" field in a Data Record header
const SyncVal uint32 = 0x0000ffff

// Header part of the Data Record Frame. For more details, consult the Reson
// Data Format Definition document.
type DataHeader struct {
	// Protocol version
	Version uint16 `json:"version"`
	// Offset in bytes from the start of the Sync Pattern to
	// the start of the Record Type Header (data section)
	Rth_offset uint16 `json:"rth_offset"`
	// Sync pattern
	Sync uint32 `json:"sync"`
	// Total size in bytes of the Data Record Frame
	Size uint32 `json:"size"`
	// Offset in bytes of optional data field
	Data_offset uint32 `json:"data_offset"`
	// User defined
	Data_id uint32 `json:"data_id"`
	// Data record time-stamp
	T Timestamp `json:"t"`
	_ [2]int8
	// Record type identifier
	Type uint32 `json:"type"`
	// Device identifier
	Device_id uint32 `json:"device_id"`
	_         [2]int8
	// Device enumerator
	Enum uint16 `json:"enum"`
	_    [4]int8
	// Set flags to 1 if checksum is used
	Flags uint16 `json:"flags"`
	_     [6]int8
	// Total records in a fragmented data record set
	Total_records uint32 `json:"total_records"`
	// Fragment number
	Frag_number uint32 `json:"frag_number"`
}

// Size of the DataHeader in bytes
var DataHeaderSize int = binary.Size(DataHeader{})

// DataRecord interface represents data record types which know how to
// unpack themselves from the data section of a Data Record Frame.
type DataRecord interface {
	// Unpack decodes the data section of a Data Record Frame
	Unpack(rdr io.Reader) error
}

type DataRecordFactory func() DataRecord

var drRegistry = map[uint32]DataRecordFactory{}

// DataRecordFrame represents the the entire RESON DRF.
type DataRecordFrame struct {
	// Header
	Hdr DataHeader `json:"hdr"`
	// Provides a Reader interface to the data section
	Data *bytes.Buffer `json:"-"`
	// Checksum
	Csum uint32 `json:"-"`
}

// ReadDataRecordFrame returns a new DataRecordFrame struct from a Reader
func ReadDataRecordFrame(rdr io.Reader) (*DataRecordFrame, error) {
	drf := DataRecordFrame{}
	err := binary.Read(rdr, ByteOrder, &drf.Hdr)
	if err == nil {
		if drf.Hdr.Sync == SyncVal {
			if LogFunc != nil {
				LogFunc(">DRF.hdr: %+v\n", drf.Hdr)
			}
			nbytes := int(drf.Hdr.Size - uint32(DataHeaderSize) - 4)
			buf := make([]byte, nbytes)
			io.ReadFull(rdr, buf)
			drf.Data = bytes.NewBuffer(buf)
			err = binary.Read(rdr, ByteOrder, &drf.Csum)
		} else {
			err = errors.New("Invalid header")
		}
	}
	return &drf, err
}

// NewDataRecordFrame constructs a new DataRecordFrame from the supplied record
// type code and a block of arbitrary data.
func NewDataRecordFrame(type_code uint, data *bytes.Buffer) *DataRecordFrame {
	hdr := DataHeader{
		Version:    uint16(5),
		Rth_offset: uint16(DataHeaderSize - 4),
		Sync:       SyncVal,
		Size:       uint32(DataHeaderSize + data.Len() + 4),
		T:          NewTimestamp(time.Now().UTC().Truncate(time.Microsecond)),
		Type:       uint32(type_code),
		Device_id:  uint32(7000),
		Enum:       uint16(0),
	}

	return &DataRecordFrame{
		Hdr:  hdr,
		Data: data,
	}
}

// ToBytes serializes a DataRecordFrame struct to a bytes Buffer
func (drf *DataRecordFrame) ToBytes() *bytes.Buffer {
	buf := new(bytes.Buffer)
	err := binary.Write(buf, ByteOrder, drf.Hdr)
	if err != nil {
		return nil
	}
	_, err = buf.Write(drf.Data.Bytes())
	if err != nil {
		return nil
	}
	err = binary.Write(buf, ByteOrder, drf.Csum)
	if err != nil {
		return nil
	}

	return buf
}

// ParseData decodes the data section of a DataRecordFrame
func (drf *DataRecordFrame) ParseData(data interface{}) error {
	return binary.Read(drf.Data, ByteOrder, data)
}

// DataRecord returns the enclosed DataRecord of a DataRecordFrame
func (drf *DataRecordFrame) GetDataRecord() (DataRecord, error) {
	f, found := drRegistry[drf.Hdr.Type]
	if !found {
		return nil, errors.New("Unregistered record type")
	}
	dr := f()
	err := dr.Unpack(drf.Data)
	return dr, err
}

// Value of the "sync" field in a Command Message
const MsgSync uint32 = 0xfe984df5

// RCMMessage is used to communicate with the Reson Command Manager and is
// not part of the 7k-protocol
type RCMMessage struct {
	// Set to MsgSync
	Sync uint32
	// Length of the message packet (20 bytes)
	Len uint32
	// Set to the characters "CTRL_CMD"
	Msg_id [8]byte
	// Index, 1-N, of the command to execute
	Cmd_id uint32
}

// NewRCMMessage creates a new RCMMessage with the specified command index
func NewRCMMessage(cmd uint) *RCMMessage {
	rm := &RCMMessage{
		Sync:   MsgSync,
		Len:    20,
		Cmd_id: uint32(cmd),
	}
	copy(rm.Msg_id[:], []byte("CTRL_CMD"))
	return rm
}

// ToBytes serializes an RCMMessage to a bytes.Buffer.
func (rm *RCMMessage) ToBytes() *bytes.Buffer {
	buf := new(bytes.Buffer)
	err := binary.Write(buf, ByteOrder, rm)
	if err != nil {
		return nil
	}
	return buf
}

// NetworkHeader is used to packetize DataFrameRecords for transmission over
// the network. See the RESON documentation for a detailed description.
type NetworkHeader struct {
	Version      uint16
	Offset       uint16
	TotalPackets uint32
	TotalRecords uint16
	Id           uint16
	Size         uint32
	TotalSize    uint32
	SeqNum       uint32
	DestId       uint32
	DestEnum     uint16
	SrcEnum      uint16
	SrcId        uint32
}

// Size of the NetworkHeader in bytes.
var NetworkHeaderSize int = binary.Size(NetworkHeader{})

// WritePackets writes a serialized Data Record Frame stored in a
// bytes.Buffer to an endpoint on the server. The endpoint is specified by
// a Device ID (dev_id) and an Enumeration value (enum).
//
// The Data Record Frame is split into one or more Network Frames
// (packets) the size of which are given by max_size. The function returns
// the number of Network Frames written along with any error that occurs.
func WritePackets(dst io.Writer, data *bytes.Buffer, max_size int,
	dev_id, enum uint) (int, error) {
	nh := NetworkHeader{
		Version:      5,
		Offset:       uint16(NetworkHeaderSize),
		TotalPackets: 1,
		TotalRecords: 1,
		Id:           1,
		TotalSize:    uint32(data.Len()),
		SeqNum:       0,
		DestId:       uint32(dev_id),
		DestEnum:     uint16(enum),
		SrcEnum:      0,
		SrcId:        0xffffffff,
	}

	var (
		packets int = data.Len() / max_size
		rem     int = data.Len() % max_size
		err     error
		n       int = 0
	)

	for err == nil && packets > 0 {
		nh.Size = uint32(max_size) + uint32(nh.Offset)
		err = binary.Write(dst, ByteOrder, &nh)
		if LogFunc != nil {
			LogFunc("<NF.hdr: %+v\n", nh)
		}
		if err != nil {
			break
		}
		b := data.Next(max_size)
		_, err = dst.Write(b)
		nh.SeqNum++
		packets--
		n++
	}

	if err == nil && rem > 0 {
		nh.Size = uint32(rem) + uint32(nh.Offset)
		err = binary.Write(dst, ByteOrder, &nh)
		if LogFunc != nil {
			LogFunc("<NF.hdr: %+v\n", nh)
		}
		if err == nil {
			b := data.Next(rem)
			_, err = dst.Write(b)
		}
		n++
	}

	return n, err
}

// PacketReader maintains state for reading data from a series
// of Network Frame packets supplied by a lower-level io.Reader.
type PacketReader struct {
	buf  []byte
	rd   io.Reader
	r, w int
}

// NewPacketReader constructs a new PacketReader instance from an
// existing io.Reader which will supply Network Frames.
func NewPacketReader(rd io.Reader) *PacketReader {
	return &PacketReader{rd: rd}
}

func (pr *PacketReader) fill_buf() error {
	var seq int = 0

	for {
		nh := NetworkHeader{}
		err := binary.Read(pr.rd, ByteOrder, &nh)
		if err != nil {
			return err
		}

		if LogFunc != nil {
			LogFunc(">NF.hdr: %+v\n", nh)
		}

		if int(nh.SeqNum) != seq {
			return errors.New("Framing error")
		}

		nbytes := int(nh.Size) - int(NetworkHeaderSize)
		if nbytes <= 0 {
			log.Printf(">NF.hdr: %+v\n", nh)
			return fmt.Errorf("Packet size error: %d", nbytes)
		}

		if seq == 0 {
			pr.buf = make([]byte, int(nh.TotalSize))
			pr.r = 0
			pr.w = 0
		}
		end := pr.w + nbytes
		if end > len(pr.buf) {
			log.Printf(">NF.hdr: %+v\n", nh)
			return fmt.Errorf("Packet size error: %d > %d", end, len(pr.buf))
		}
		n, err := io.ReadFull(pr.rd, pr.buf[pr.w:end])
		if err != nil {
			return err
		}
		pr.w += n
		if pr.w >= int(nh.TotalSize) {
			break
		}
		seq++
	}

	return nil
}

// Read data from Network Frame packets into p. The Network Frame
// headers are discarded.
func (pr *PacketReader) Read(p []byte) (int, error) {
	n := len(p)
	if n == 0 {
		return 0, nil
	}

	if pr.r == pr.w {
		err := pr.fill_buf()
		if err != nil {
			return 0, err
		}
	}

	n = copy(p, pr.buf[pr.r:pr.w])
	pr.r += n
	return n, nil
}
