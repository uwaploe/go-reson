package reson

import (
	"bytes"
	"log"
	"testing"
)

const (
	DEV_ID   uint = 7000
	DEV_ENUM uint = 0
)

func comp_bytes(a, b []byte) (bool, int) {
	if len(a) != len(b) {
		return false, -1
	}

	n := len(b)
	for i, x := range a {
		if i >= n || x != b[i] {
			return false, i
		}
	}
	return true, 0
}

func TestDataRecord(t *testing.T) {
	if testing.Verbose() {
		LogFunc = log.Printf
	}
	dbuf := new(bytes.Buffer)
	code, err := CmdSubscribe(dbuf, []uint32{7000, 7008, 7038})
	if err != nil {
		t.Errorf("Cannot generate subscribe packet: %v", err)
	}
	drf := NewCommand(code, dbuf)
	// Buffer to hold generated Network Frames
	obuf := new(bytes.Buffer)
	save := make([]byte, drf.ToBytes().Len())
	copy(save, drf.ToBytes().Bytes())
	_, err = WritePackets(obuf, drf.ToBytes(), 32, DEV_ID, DEV_ENUM)
	if err != nil {
		t.Errorf("Cannot write network frames: %v", err)
	}

	if testing.Verbose() {
		log.Printf("Output length: %d bytes\n", obuf.Len())
	}

	rdr := NewPacketReader(obuf)
	// Parse the data record from the packets
	drf2, err := ReadDataRecordFrame(rdr)

	if err != nil {
		t.Errorf("Error reading data record: %v", err)
	}

	state, idx := comp_bytes(save, drf2.ToBytes().Bytes())

	if !state {
		t.Errorf("Data mismatch at byte %d (%v != %v)", idx, save, drf2.ToBytes().Bytes())
	}
}
