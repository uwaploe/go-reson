package reson

import (
	"bytes"
	"io"
	"testing"

	"github.com/satori/go.uuid"
)

// We don't test every command function, just a representative sample, i.e. a
// mix of argument types.
var Tests = []struct {
	Name      string
	Uid       string
	Packed    string
	Generator func(w io.Writer) (uint32, error)
}{
	{
		"reboot",
		"cfa96bb5-48d7-43b4-bdb8-dfc825dc2891",
		"\xe9\x03\x00\x00\x01\x00\x00\x00\xcf\xa9k\xb5H\xd7C\xb4\xbd\xb8\xdf\xc8%\xdc(\x91",
		func(w io.Writer) (uint32, error) {
			return CmdReboot(w)
		},
	},
	{
		"range",
		"5dfee713-5e61-4cc4-aa07-26e01eeef57a",
		"\xeb\x03\x00\x00\x01\x00\x00\x00]\xfe\xe7\x13^aL\xc4\xaa\x07&\xe0\x1e\xee\xf5z\x00\x00\x00B",
		func(w io.Writer) (uint32, error) {
			return CmdSetRange(w, 32.0)
		},
	},
	{
		"max_ping_rate",
		"18460d20-25c6-4e0f-87ad-28f7049ffb5b",
		"\xec\x03\x00\x00\x01\x00\x00\x00\x18F\r %\xc6N\x0f\x87\xad(\xf7\x04\x9f\xfb[\x00\x00\x80?",
		func(w io.Writer) (uint32, error) {
			return CmdMaxPingRate(w, 1.0)
		},
	},
	{
		"transmit_power",
		"d286d508-db71-4a56-9961-a886d5f03afa",
		"\xed\x03\x00\x00\x01\x00\x00\x00\xd2\x86\xd5\x08\xdbqJV\x99a\xa8\x86\xd5\xf0:\xfa\x00\x00\x00C",
		func(w io.Writer) (uint32, error) {
			return CmdTransmitPower(w, 128.0)
		},
	},
	{
		"pulse_width",
		"629d8874-fa66-4c29-b2ab-0aff66c7b906",
		"\xee\x03\x00\x00\x01\x00\x00\x00b\x9d\x88t\xfafL)\xb2\xab\n\xfff\xc7\xb9\x06\x00\x00\x80?",
		func(w io.Writer) (uint32, error) {
			return CmdPulseWidth(w, 1.0)
		},
	},
	{
		"pulse_type",
		"bab576d7-63a7-41c0-b71b-3759a3f5daac",
		"\xef\x03\x00\x00\x01\x00\x00\x00\xba\xb5v\xd7c\xa7A\xc0\xb7\x1b7Y\xa3\xf5\xda\xac\x01\x00\x00\x00",
		func(w io.Writer) (uint32, error) {
			return CmdPulseType(w, PT_CHIRP)
		},
	},
	{
		"gain",
		"fe65cf95-25db-4522-b09e-beca4323146e",
		"\xf0\x03\x00\x00\x01\x00\x00\x00\xfee\xcf\x95%\xdbE\x22\xb0\x9e\xbe\xcaC#\x14n\x00\x00\x00@",
		func(w io.Writer) (uint32, error) {
			return CmdGain(w, 2.0)
		},
	},
	{
		"gain_type",
		"c84d4086-3b43-4dd8-888c-8f3a3f264b67",
		"\xf9\x03\x00\x00\x01\x00\x00\x00\xc8M@\x86;CM\xd8\x88\x8c\x8f:?&Kg\x02\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00",
		func(w io.Writer) (uint32, error) {
			return CmdGainType(w, GT_FIXED)
		},
	},
	{
		"subscribe",
		"f1d1f8b9-cb6f-41f5-9d02-d06d507da6b4",
		"\x1b\x04\x00\x00\x01\x00\x00\x00\xf1\xd1\xf8\xb9\xcboA\xf5\x9d\x02\xd0mP}\xa6\xb4\x02\x00\x00\x00X\x1b\x00\x00~\x1b\x00\x00",
		func(w io.Writer) (uint32, error) {
			return CmdSubscribe(w, []uint32{7000, 7038})
		},
	},
	{
		"set_directory",
		"90a30e99-74c9-4fcb-989d-dcbc32d67e36",
		"\xb7\x04\x00\x00\x01\x00\x00\x00\x90\xa3\x0e\x99t\xc9O\xcb\x98\x9d\xdc\xbc2\xd6~6d:\\\\Data\\Calibration 2018\x00",
		func(w io.Writer) (uint32, error) {
			return CmdSetDirectory(w, "d://Data/Calibration 2018")
		},
	},
	{
		"set_filtering",
		"768bf7d8-d6ea-4eb8-af6a-e40891f6f208",
		"\xb9\x04\x00\x00\x01\x00\x00\x00v\x8b\xf7\xd8\xd6\xeaN\xb8\xafj\xe4\x08\x91\xf6\xf2\x08\x00\x00\x01\x00\x02\x00X\x1b\x00\x00~\x1b\x00\x00",
		func(w io.Writer) (uint32, error) {
			return CmdSetRecordFilter(w, []uint32{7000, 7038})
		},
	},
}

func TestCommands(t *testing.T) {
	for _, test := range Tests {
		buf := new(bytes.Buffer)
		code, err := test.Generator(buf)
		if err != nil {
			t.Errorf("Command %s test failed: %v", test.Name, err)
		}
		u, err := uuid.FromString(test.Uid)
		rc := &RemoteCmd{
			Hdr: RemoteCmdHeader{
				Command: code,
				Ticket:  1,
				Uid:     u,
			},
			Data: buf,
		}
		p := rc.ToBytes().Bytes()
		if string(p) != test.Packed {
			t.Errorf("Packet mismatch: %q != %q", p, test.Packed)
		}
	}
}
