package reson

import (
	"bytes"
	"context"
	"log"
	"net"
	"time"

	"github.com/pkg/errors"
)

// Conn represents a connection to the Reson server (7kCenter)
type Conn interface {
	// Close closes the connection
	Close() error
	// Clone returns a copy of the connection.
	Clone() (Conn, error)
	// SetEndpoint sets the device ID and system enumerator for the
	// particular 7kCenter device that we need to communicate with.
	SetEndpoint(dev_id, dev_enum uint)
	// SetParams sets the sonar data acquisition parameters
	SetParams(p SonarParams) error
	// Send sends a Data Record Frame to the server
	Send(drf *DataRecordFrame) error
	// Recv reads a single DataRecordFrame from the server. If skip_ack is
	// True and the next record is a command ACK, that record is skipped
	// and the subsequent record is returned.
	Recv(skip_ack bool) (*DataRecordFrame, error)
	// Stream starts a goroutine which subscribes to the data record types
	// given in rtypes then reads DataRecordFrames from the server
	// continuously and writes them to a channel. Any errors will cause
	// the goroutine to exit and close the channel. The supplied Context
	// can be used to force the goroutine to exit and close the channel.
	Stream(ctx context.Context) (<-chan *DataRecordFrame, error)
	// Subscribe sends a Record Subscription command to the server.
	Subscribe(rtypes []uint32) error
	// Unsubscribe stops the subscription to all records
	Unsubscribe() error
	// ReqRecord requests a single record from the server
	ReqRecord(rtype uint32) (*DataRecordFrame, error)
	// GetDevices returns a list of the available devices on the server
	GetDevices() ([]DevInfo, error)
	// StartRecording starts the data record recording process on the server
	StartRecording(dirname string, rtypes []uint32) error
	// RecFilter sets the data recording filter on the server
	RecFilter(rtypes []uint32) error
	// StopRecording stops the data record recording process
	StopRecording() error
	// GetStorageStatus returns information about data record storage on
	// the server.
	GetStorageStatus() (*StorageStatus, error)
	// RecordTypes returns a list of record types available for subscription
	// or recording
	RecordTypes() ([]uint32, error)
	// RawMode enables (true) or disables (false) the collection of raw
	// (non-beamformed) I and Q data
	RawMode(state bool) error
	// Start starts continuous pinging by the sonar. Rate is the maximum
	// ping rate in Hz.
	Start(rate float32) error
	// Stop stops continuous pinging
	Stop() error
	// Shutdown the server
	Shutdown() error
}

// SonarParams represents the value passed to Conn.SetParams
type SonarParams struct {
	// Receiver gain in dB
	RxGain float32 `json:"rxgain" toml:"rxgain,omitempty"`
	// Transmit power in dB
	TxPower float32 `json:"txpower" toml:"txpower,omitempty"`
	// Transmit pulse width in seconds
	PulseWidth float32 `json:"pulse_width" toml:"pw,omitempty"`
	// System range setting in meters
	Range float32 `json:"range" toml:"range,omitempty"`
}

type conn struct {
	conn         net.Conn
	dev_id       uint
	dev_enum     uint
	read_timeout time.Duration
	raw_mode     bool
}

// Maximum Network Frame packet size
const MaxPacketSize int = 60000

// RcmRun sends a command message to the Reson Command Manager (RCM). The
// RCM is a limited "remote shell" which allows execution of a
// predeterimed set of commands, the RCM.ini file on the server contains
// the command list. The command code supplied to this function is an
// index into that list.
//
// The primary function of the RCM is to start versions of the 7kCenter
// with different hardware configurations.
func RcmRun(address string, cmd_code uint) error {
	addr, err := net.ResolveTCPAddr("tcp4", address)
	if err != nil {
		return err
	}
	conn, err := net.DialTCP("tcp", nil, addr)
	if err != nil {
		return err
	}
	conn.SetNoDelay(true)
	msg := NewRCMMessage(cmd_code).ToBytes().Bytes()
	conn.SetWriteDeadline(time.Now().Add(5 * time.Second))
	_, err = conn.Write(msg)
	if err != nil {
		return errors.Wrap(err, "write")
	}

	return conn.Close()
}

// Construct an error from a NAK record.
func error_from_nak(drf *DataRecordFrame) (err error) {
	v := RemoteNak{}
	err = drf.ParseData(&v)
	if err == nil {
		err = errors.Errorf("NAK code: 0x%x", v.Error_code)
	}
	return
}

// NewConn returns a new Reson connection for the given net connection
func NewConn(netConn net.Conn, read_timeout time.Duration) Conn {
	return &conn{
		conn:         netConn,
		dev_id:       7000,
		dev_enum:     0,
		read_timeout: read_timeout,
	}
}

// Dial connects to a Reson 7kCenter server at the given network and address
func Dial(address string, read_timeout time.Duration) (Conn, error) {
	addr, err := net.ResolveTCPAddr("tcp4", address)
	if err != nil {
		return nil, err
	}
	conn, err := net.DialTCP("tcp", nil, addr)
	if err != nil {
		return nil, err
	}

	return NewConn(conn, read_timeout), nil
}

func (c *conn) Clone() (Conn, error) {
	addr := c.conn.RemoteAddr()
	new_c, err := Dial(addr.String(), c.read_timeout)
	if err != nil {
		return nil, err
	}
	new_c.SetEndpoint(c.dev_id, c.dev_enum)
	return new_c, nil
}

func (c *conn) send_command(code uint32,
	data *bytes.Buffer, skip_ack bool) (*DataRecordFrame, error) {
	drf := NewCommand(code, data)
	err := c.Send(drf)
	if err != nil {
		return nil, errors.Wrap(err, "DRF send")
	}
	reply, err := c.Recv(skip_ack)
	if reply.Hdr.Type == RTNak {
		err = error_from_nak(reply)
	}

	return reply, err
}

func (c *conn) Close() error {
	return c.conn.Close()
}

func (c *conn) SetEndpoint(dev_id, dev_enum uint) {
	c.dev_id = dev_id
	c.dev_enum = dev_enum
}

func (c *conn) Send(drf *DataRecordFrame) error {
	drf.Hdr.Device_id = uint32(c.dev_id)
	drf.Hdr.Enum = uint16(c.dev_enum)
	_, err := WritePackets(c.conn, drf.ToBytes(), MaxPacketSize, c.dev_id, c.dev_enum)
	return err
}

func (c *conn) Recv(skip_ack bool) (*DataRecordFrame, error) {
	if c.read_timeout != 0 {
		c.conn.SetReadDeadline(time.Now().Add(c.read_timeout))
	}

	rdr := NewPacketReader(c.conn)
	// Parse the data record from the packets
	drf, err := ReadDataRecordFrame(rdr)
	if skip_ack && drf.Hdr.Type == RTAck {
		// Skip the ACK and return the command response
		drf, err = ReadDataRecordFrame(rdr)
	}

	return drf, err
}

func (c *conn) Stream(ctx context.Context) (<-chan *DataRecordFrame, error) {
	ch := make(chan *DataRecordFrame, 1)
	go func() {
		defer close(ch)
		defer func() {
			if x := recover(); x != nil {
				log.Printf("PANIC: %v", x)
			}
		}()
		if c.read_timeout != 0 {
			c.conn.SetReadDeadline(time.Now().Add(c.read_timeout))
		}
		rdr := NewPacketReader(c.conn)
		for {
			drf, err := ReadDataRecordFrame(rdr)
			if err != nil {
				return
			}
			if c.read_timeout != 0 {
				c.conn.SetReadDeadline(time.Now().Add(c.read_timeout))
			}
			select {
			case <-ctx.Done():
				return
			default:
				ch <- drf
			}
		}
	}()

	return ch, nil
}

func (c *conn) Subscribe(rtypes []uint32) error {
	dbuf := new(bytes.Buffer)
	code, err := CmdSubscribe(dbuf, rtypes)
	if err != nil {
		return errors.Wrap(err, "building command packet")
	}
	_, err = c.send_command(code, dbuf, false)
	return err
}

func (c *conn) Unsubscribe() error {
	dbuf := new(bytes.Buffer)
	code, err := CmdUnsubscribe(dbuf)
	if err != nil {
		return errors.Wrap(err, "building command packet")
	}
	_, err = c.send_command(code, dbuf, false)
	return err
}

func (c *conn) ReqRecord(rtype uint32) (*DataRecordFrame, error) {
	dbuf := new(bytes.Buffer)
	code, err := CmdRecordReq(dbuf, rtype)
	if err != nil {
		return nil, errors.Wrap(err, "building command packet")
	}

	return c.send_command(code, dbuf, true)
}

func (c *conn) GetDevices() ([]DevInfo, error) {
	drf, err := c.ReqRecord(RTConfiguration)
	if err != nil {
		return nil, err
	}
	cf, err := ReadConfig(drf.Data)
	if err != nil {
		return nil, err
	}

	return cf.DeviceInfo(), nil
}

func (c *conn) StartRecording(dirname string, rtypes []uint32) error {
	dbuf := new(bytes.Buffer)
	code, err := CmdSetDirectory(dbuf, dirname)
	if err != nil {
		return errors.Wrap(err, "set directory")
	}
	_, err = c.send_command(code, dbuf, false)
	if err != nil {
		return errors.Wrap(err, "set directory")
	}
	dbuf.Reset()

	code, err = CmdSetRecordFilter(dbuf, rtypes)
	if err != nil {
		return errors.Wrap(err, "set record filter")
	}
	_, err = c.send_command(code, dbuf, false)
	if err != nil {
		return errors.Wrap(err, "set record filter")
	}
	dbuf.Reset()

	filename := time.Now().UTC().Format("20060102_150405")
	code, err = CmdStartRecording(dbuf, filename)
	if err != nil {
		return errors.Wrap(err, "start recording")
	}
	_, err = c.send_command(code, dbuf, false)
	return err
}

func (c *conn) RecFilter(rtypes []uint32) error {
	dbuf := new(bytes.Buffer)
	code, err := CmdSetRecordFilter(dbuf, rtypes)
	if err != nil {
		return errors.Wrap(err, "set record filter")
	}
	_, err = c.send_command(code, dbuf, false)
	return err
}

func (c *conn) StopRecording() error {
	dbuf := new(bytes.Buffer)
	code, err := CmdStopRecording(dbuf)
	if err != nil {
		return err
	}
	_, err = c.send_command(code, dbuf, false)
	return err
}

func (c *conn) GetStorageStatus() (*StorageStatus, error) {
	drf, err := c.ReqRecord(RTStorageStatus)
	if err != nil {
		return nil, err
	}
	return ReadStorageStatus(drf.Data)
}

func (c *conn) RecordTypes() ([]uint32, error) {
	dbuf := new(bytes.Buffer)
	code, err := CmdSetRecordFilter(dbuf, nil)
	if err != nil {
		return nil, errors.Wrap(err, "set record filter")
	}
	_, err = c.send_command(code, dbuf, false)
	if err != nil {
		return nil, errors.Wrap(err, "set record filter")
	}

	s, err := c.GetStorageStatus()
	if err != nil {
		return nil, err
	}

	return s.IncludedRecords.Vals, nil
}

func (c *conn) SetParams(p SonarParams) error {
	dbuf := new(bytes.Buffer)

	code, err := CmdGain(dbuf, p.RxGain)
	if err != nil {
		return errors.Wrap(err, "set gain")
	}
	_, err = c.send_command(code, dbuf, false)
	if err != nil {
		return errors.Wrap(err, "set gain")
	}
	dbuf.Reset()

	code, err = CmdTransmitPower(dbuf, p.TxPower)
	if err != nil {
		return errors.Wrap(err, "set power")
	}
	_, err = c.send_command(code, dbuf, false)
	if err != nil {
		return errors.Wrap(err, "set power")
	}
	dbuf.Reset()

	code, err = CmdPulseWidth(dbuf, p.PulseWidth)
	if err != nil {
		return errors.Wrap(err, "set pulse width")
	}
	_, err = c.send_command(code, dbuf, false)
	if err != nil {
		return errors.Wrap(err, "set pulse width")
	}
	dbuf.Reset()

	code, err = CmdSetRange(dbuf, p.Range)
	if err != nil {
		return errors.Wrap(err, "set range")
	}
	_, err = c.send_command(code, dbuf, false)
	if err != nil {
		return errors.Wrap(err, "set range")
	}

	return nil
}

func (c *conn) RawMode(state bool) error {
	dbuf := new(bytes.Buffer)
	var mode CenterMode = CM_NORMAL
	if state {
		mode = CM_RAWIQ
	}
	code, err := CmdCenterMode(dbuf, mode, 0)
	if err != nil {
		return errors.Wrap(err, "center mode")
	}
	_, err = c.send_command(code, dbuf, false)
	return err
}

func (c *conn) Start(rate float32) error {
	dbuf := new(bytes.Buffer)
	code, err := CmdMaxPingRate(dbuf, rate)
	if err != nil {
		return errors.Wrap(err, "ping rate")
	}
	_, err = c.send_command(code, dbuf, false)
	if err != nil {
		return err
	}
	dbuf.Reset()

	code, err = CmdStartPing(dbuf)
	if err != nil {
		return errors.Wrap(err, "start ping")
	}
	_, err = c.send_command(code, dbuf, false)

	return err
}

func (c *conn) Stop() error {
	dbuf := new(bytes.Buffer)

	// Due to a bug in 7kCenter, we need to set the MaxPingRate to zero
	// before sending a Stop Pinging command.
	code, err := CmdMaxPingRate(dbuf, float32(0))
	if err != nil {
		return errors.Wrap(err, "ping rate")
	}
	_, err = c.send_command(code, dbuf, false)
	if err != nil {
		return err
	}
	dbuf.Reset()

	code, err = CmdStopPing(dbuf)
	if err != nil {
		return errors.Wrap(err, "stop ping")
	}
	_, err = c.send_command(code, dbuf, false)
	return err
}

func (c *conn) Shutdown() error {
	var err error
	dbuf := new(bytes.Buffer)

	// Set the transmit power level to zero before shutting
	// the server down. Ignore errors because we are going
	// to send the StopProcess command regardless.
	code, _ := CmdTransmitPower(dbuf, 0)
	c.send_command(code, dbuf, false)
	dbuf.Reset()

	code, err = CmdStopProcess(dbuf)
	if err != nil {
		return errors.Wrap(err, "shutdown")
	}
	_, err = c.send_command(code, dbuf, false)
	if err != nil {
		return errors.Wrap(err, "shutdown")
	}
	c.Close()
	return nil
}
