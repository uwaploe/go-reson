package reson

import (
	"encoding/binary"
	"io"
)

// Reson 7K Beamformed Data record header, type 7018
type BeamformedHeader struct {
	Sonar_id   uint64 `json:"sonar_id"`
	Ping_num   uint32 `json:"ping_num"`
	Multi_ping uint16 `json:"multi_ping"`
	Nbeams     uint16 `json:"nbeams"`
	Nsamples   uint32 `json:"nsamples"`
	_          [8]uint32
}

func init() {
	drRegistry[uint32(7018)] = func() DataRecord { return &Beamformed{} }
}

type Beamformed struct {
	Hdr  RawIQHeader `json:"hdr"`
	Data []byte      `json:"-"`
}

func (hdr *BeamformedHeader) DataSize() int {
	return int(hdr.Nsamples) * int(hdr.Nbeams) * 4
}

// Unpack implements the reson.DataRecord interface
func (bf *Beamformed) Unpack(rdr io.Reader) error {
	err := binary.Read(rdr, ByteOrder, &bf.Hdr)
	if err == nil {
		bf.Data = make([]byte, bf.Hdr.DataSize())
		_, err = io.ReadFull(rdr, bf.Data)
	}
	return err
}

// ReadBeamformed returns a new Beamformed struct from a data source.
func ReadBeamformed(rdr io.Reader) (*Beamformed, error) {
	bf := &Beamformed{}
	err := bf.Unpack(rdr)
	return bf, err
}
