package reson

import (
	"encoding/binary"
	"io"
)

// Reson 7K Raw IQ Data record header, type 7038.
type RawIQHeader struct {
	Sonar_id     uint64 `json:"sonar_id"`
	Ping_num     uint32 `json:"ping_num"`
	_            uint16
	Total_elem   uint16 `json:"total_elem"`
	Samples      uint32 `json:"samples"`
	Reduced_elem uint16 `json:"reduced_elem"`
	Start        uint32 `json:"start"`
	Stop         uint32 `json:"stop"`
	Samp_type    uint16 `json:"samp_type"`
	_            [28]uint8
}

type IQSample struct {
	I, Q uint16
}

type RawIQ struct {
	Hdr      RawIQHeader `json:"hdr"`
	Elements []uint16    `json:"elements"`
	Data     []IQSample  `json:"-"`
}

func init() {
	drRegistry[uint32(7038)] = func() DataRecord { return &RawIQ{} }
}

func (hdr *RawIQHeader) DataSize() int {
	var nsamples int
	if hdr.Stop >= hdr.Samples {
		nsamples = int(hdr.Samples)
	} else {
		nsamples = int(hdr.Stop - hdr.Start + 1)
	}
	return int(hdr.Reduced_elem) * nsamples
}

// Unpack implements the reson.DataRecord interface
func (rf *RawIQ) Unpack(rdr io.Reader) error {
	err := binary.Read(rdr, ByteOrder, &rf.Hdr)
	if err == nil {
		rf.Elements = make([]uint16, rf.Hdr.Reduced_elem)
		rf.Data = make([]IQSample, rf.Hdr.DataSize())
		binary.Read(rdr, ByteOrder, rf.Elements)
		err = binary.Read(rdr, ByteOrder, rf.Data)
	}
	return err
}

// ReadRawIQ returns a new RawIQ struct from a data source.
func ReadRawIQ(rdr io.Reader) (*RawIQ, error) {
	rf := &RawIQ{}
	err := rf.Unpack(rdr)
	return rf, err
}

// Pack writes the RawIQ struct in raw binary format.
func (rf *RawIQ) Pack(w io.Writer) error {
	err := binary.Write(w, ByteOrder, rf.Hdr)
	if err != nil {
		return err
	}
	binary.Write(w, ByteOrder, rf.Elements)
	return binary.Write(w, ByteOrder, rf.Data)
}

// Dataset returns the data values as a slice of complex numbers
func (rf *RawIQ) Dataset() []complex64 {
	n := len(rf.Data)
	ds := make([]complex64, n)

	var cvt func(x uint16) float32
	if rf.Hdr.Samp_type == 16 {
		cvt = func(x uint16) float32 { return float32(int16(x) >> 4) }
	} else {
		cvt = func(x uint16) float32 { return float32(int16(x<<4) >> 4) }
	}

	for i := 0; i < n; i++ {
		ds[i] = complex(cvt(rf.Data[i].I), cvt(rf.Data[i].Q))
	}

	return ds
}
