package reson

import (
	"bytes"
	"encoding/binary"
	"encoding/json"
	"io"
	"os"
	"path/filepath"
	"testing"
)

func helperOpenFile(t *testing.T, name string) *os.File {
	path := filepath.Join("testdata", name)
	f, err := os.Open(path)
	if err != nil {
		t.Fatal(err)
	}
	return f
}

func TestRawIQ(t *testing.T) {
	f := helperOpenFile(t, "rec_7038.bin")
	defer f.Close()
	rec, err := ReadRawIQ(f)
	if err != nil {
		t.Fatal(err)
	}

	if len(rec.Data) != rec.Hdr.DataSize() {
		t.Errorf("Bad data size: %d != %d", len(rec.Data),
			rec.Hdr.DataSize())
	}

	n := len(rec.Elements) - 1
	if rec.Elements[n] != rec.Hdr.Total_elem-1 {
		t.Errorf("Bad last element #; got %d, expected %d", rec.Elements[n],
			rec.Hdr.Total_elem-1)
	}

	if rec.Hdr.Ping_num != 508 {
		t.Errorf("Wrong ping number: %d != 508", rec.Hdr.Ping_num)
	}

	if rec.Hdr.Samples != 3460 {
		t.Errorf("Wrong sample count: %d != 3460", rec.Hdr.Samples)
	}

}

func TestSettings(t *testing.T) {
	f := helperOpenFile(t, "rec_7000.json")
	defer f.Close()
	dec := json.NewDecoder(f)

	var s Settings
	err := dec.Decode(&s)
	if err != nil && err != io.EOF {
		t.Fatal(err)
	}

	if (s.Hdr.Control & 0x100) != 0x100 {
		t.Errorf("Bad value for Control_flags: 0x%08x", s.Hdr.Control)
	}
}

func TestRCM(t *testing.T) {
	output := []byte("\xf5M\x98\xfe\x14\x00\x00\x00CTRL_CMD\x01\x00\x00\x00")
	rm := NewRCMMessage(1)
	if rm == nil {
		t.Error("Cannot create RCMMessage struct")
	}
	if !bytes.Equal(output, rm.ToBytes().Bytes()) {
		t.Errorf("Format mismatch: %q != %q", output,
			rm.ToBytes().Bytes())
	}
}

func TestTimestamp(t *testing.T) {
	b := bytes.NewBuffer([]byte{0xda, 0x07, 0x80, 0x00, 0x9b, 0x6b, 0x94, 0x41, 0x07, 0x3b})
	ts := Timestamp{}
	err := binary.Read(b, ByteOrder, &ts)
	if err != nil {
		t.Fatal(err)
	}
	expected := "2010-05-08 07:59:18.552541 +0000 UTC"
	if ts.Time().String() != expected {
		t.Errorf("Timestamp conversion error: %v", ts.Time())
	}
}
