package reson

import (
	"bytes"
	"encoding/binary"
	"io"

	"github.com/pkg/errors"
	"github.com/satori/go.uuid"
)

type PulseType uint32

const (
	PT_CW PulseType = iota
	PT_CHIRP
)

type CenterMode uint16

const (
	CM_NORMAL CenterMode = iota
	CM_AUTOPILOT
	CM_RAWIQ
)

type GainType uint32

const (
	GT_TVG GainType = iota
	GT_AUTOGAIN
	GT_FIXED
)

type EnvelopeType uint32

const (
	ET_RECTANGULAR EnvelopeType = iota
	ET_TUKEY
)

type FilterType uint32

const (
	FT_RECTANGULAR FilterType = iota
	FT_KAISER
	FT_HAMMING
	FT_BLACKMAN
	FT_TRIANGULAR
	FT_TAYLOR
)

// Header for record type 7500
type RemoteCmdHeader struct {
	// Remote control ID (command code)
	Command uint32 `json:"command"`
	// Ticket number (returned in ACK or NAK packets)
	Ticket uint32 `json:"ticket"`
	// Unique ID set by client
	Uid uuid.UUID `json:"uid"`
}

// Full Remote Command record
type RemoteCmd struct {
	Hdr  RemoteCmdHeader
	Data *bytes.Buffer
}

// Record type 7501
type RemoteAck struct {
	Ticket uint32    `json:"ticket"`
	Uid    uuid.UUID `json:"uid"`
}

// Record type 7502
type RemoteNak struct {
	Ticket     uint32    `json:"ticket"`
	Uid        uuid.UUID `json:"uid"`
	Error_code uint32    `json:"error_code"`
}

// NewCommand creates a DataRecordFrame encapsulating a serialized RemoteCmd struct
func NewCommand(code uint32, data *bytes.Buffer) *DataRecordFrame {
	rc := &RemoteCmd{
		Hdr: RemoteCmdHeader{
			Command: code,
			Ticket:  1,
			Uid:     uuid.NewV4(),
		},
		Data: data,
	}
	if LogFunc != nil {
		LogFunc("<Command: %+v\n", rc)
	}
	return NewDataRecordFrame(7500, rc.ToBytes())
}

// ToBytes serializes a RemoteCmd struct to a bytes Buffer
func (rc *RemoteCmd) ToBytes() *bytes.Buffer {
	buf := new(bytes.Buffer)
	err := binary.Write(buf, ByteOrder, rc.Hdr)
	if err != nil {
		return nil
	}
	_, err = buf.Write(rc.Data.Bytes())
	if err != nil {
		return nil
	}
	return buf
}

// Command functions
//
// Each function returns an integer command code and writes the
// command parameters to the supplied io.Writer.
//
func write_command(w io.Writer, params ...interface{}) error {
	for _, p := range params {
		err := binary.Write(w, ByteOrder, p)
		if err != nil {
			return errors.Wrapf(err, "Cannot pack value: %v")
		}
	}
	return nil
}

// CmdShutdown shuts-down the entire Reson system
func CmdShutdown(w io.Writer) (uint32, error) {
	return uint32(1000), nil
}

// CmdReboot resets the Reson software and firmware
func CmdReboot(w io.Writer) (uint32, error) {
	return uint32(1001), nil
}

// CmdSetRange sets the system range in meters
func CmdSetRange(w io.Writer, meters float32) (uint32, error) {
	return uint32(1003), write_command(w, meters)
}

// CmdMaxPingRate sets the number of pings per second
func CmdMaxPingRate(w io.Writer, hz float32) (uint32, error) {
	return uint32(1004), write_command(w, hz)
}

// CmdTransmitPower sets the transmitter power level in decibels
func CmdTransmitPower(w io.Writer, db float32) (uint32, error) {
	return uint32(1005), write_command(w, db)
}

// CmdPulseWidth sets the transmit pulse width in seconds
func CmdPulseWidth(w io.Writer, secs float32) (uint32, error) {
	return uint32(1006), write_command(w, secs)
}

// CmdPulseType sets the transmit pulse type
func CmdPulseType(w io.Writer, pt PulseType) (uint32, error) {
	return uint32(1007), write_command(w, uint32(pt))
}

// CmdGain sets the receiver gain in decibels
func CmdGain(w io.Writer, db float32) (uint32, error) {
	return uint32(1008), write_command(w, db)
}

// CmdCenterMode sets the mode of the 7kCenter program
func CmdCenterMode(w io.Writer, mode CenterMode, method uint16) (uint32, error) {
	return uint32(1014), write_command(w, uint16(mode), method)
}

// CmdGainType sets the receiver gain type
func CmdGainType(w io.Writer, gt GainType) (uint32, error) {
	padding := make([]uint32, 4)
	return uint32(1017), write_command(w, uint32(gt), padding)
}

// CmdPulseEnvelope sets the transmit pulse envelope parameters
func CmdPulseEnvelope(w io.Writer, et EnvelopeType, shading float32) (uint32, error) {
	padding := make([]uint32, 4)
	return uint32(1020), write_command(w, et, shading, padding)
}

// CmdProjectorSteering sets the beam steering in radians
func CmdProjectorSteering(w io.Writer, horiz, vert float32) (uint32, error) {
	return uint32(1021), write_command(w, horiz, vert)
}

// CmdProjectorWidth sets the 3dB beam widths in radians
func CmdProjectorWidth(w io.Writer, horiz, vert float32) (uint32, error) {
	return uint32(1022), write_command(w, horiz, vert)
}

// CmdChirpFreq sets the chirp start and stop frequencies in Hz
func CmdChirpFreq(w io.Writer, start, stop float32) (uint32, error) {
	return uint32(1027), write_command(w, start, stop)
}

// CmdProjectorLoc sets the x,y,z offset of the transmitter in meters
func CmdProjectorLoc(w io.Writer, x, y, z float32) (uint32, error) {
	return uint32(1029), write_command(w, x, y, z)
}

// CmdMatchFilter sets the match filter parameters
func CmdMatchFilter(w io.Writer, ft FilterType, shading float32) (uint32, error) {
	padding := make([]uint32, 4)
	return uint32(1034), write_command(w, uint32(ft), shading, padding)
}

// CmdRecordReq requests the most recent data Record of the specified type
func CmdRecordReq(w io.Writer, rt uint32) (uint32, error) {
	return uint32(1050), write_command(w, rt)
}

// CmdSubscribe subscribes the client to one or more data record types
func CmdSubscribe(w io.Writer, rts []uint32) (uint32, error) {
	return uint32(1051), write_command(w, uint32(len(rts)), rts)
}

// CmdUnsubscribe unsubscribes to all records
func CmdUnsubscribe(w io.Writer) (uint32, error) {
	return uint32(1052), nil
}

// CmdStopProcess stops the 7kCenter server process
func CmdStopProcess(w io.Writer) (uint32, error) {
	return uint32(1099), nil
}

// CmdStartPing starts continuous pinging
func CmdStartPing(w io.Writer) (uint32, error) {
	return uint32(1100), nil
}

// CmdStopPing stops pinging
func CmdStopPing(w io.Writer) (uint32, error) {
	return uint32(1101), nil
}

// CmdBeamLimit limits the number of beams in record 7008
func CmdBeamLimit(w io.Writer, min, max uint32) (uint32, error) {
	return uint32(1104), write_command(w, min, max)
}

// CmdSinglePing requests a single ping
func CmdSinglePing(w io.Writer) (uint32, error) {
	return uint32(1107), nil
}

// CmdSysVerify requests an ACK (used to verify network communication)
func CmdSysVerify(w io.Writer) (uint32, error) {
	return uint32(1109), nil
}

// CmdExtTrigger enables pinging on an external trigger
func CmdExtTrigger(w io.Writer) (uint32, error) {
	return uint32(1114), nil
}

// CmdStartRecording enables data record logging on the Reson system
func CmdStartRecording(w io.Writer, filename string) (uint32, error) {
	path := make([]byte, 0, 256)
	for _, s := range filename {
		if s == 0x2f {
			path = append(path, byte(0x5c))
		} else {
			path = append(path, byte(s))
		}
	}
	path = append(path, uint8(0))
	return uint32(1200), write_command(w, uint32(0), path)
}

// CmdStopRecording enables data record logging on the Reson system
func CmdStopRecording(w io.Writer) (uint32, error) {
	return uint32(1201), nil
}

// CmdSetDirectory sets the data log storage directory on the Reson
// system. Windows path format.
func CmdSetDirectory(w io.Writer, dirname string) (uint32, error) {
	path := make([]byte, 0, 256)
	for _, s := range dirname {
		if s == 0x2f {
			path = append(path, byte(0x5c))
		} else {
			path = append(path, byte(s))
		}
	}
	path = append(path, uint8(0))
	return uint32(1207), write_command(w, path)
}

// CmdSetRecordFilter specifies the data record types to record.
func CmdSetRecordFilter(w io.Writer, rts []uint32) (uint32, error) {
	if rts == nil || len(rts) == 0 {
		return uint32(1209), write_command(w, uint16(0), uint16(0), uint16(0))
	} else {
		return uint32(1209), write_command(w, uint16(0), uint16(1),
			uint16(len(rts)), rts)
	}
}
