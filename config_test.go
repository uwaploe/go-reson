package reson

import (
	"testing"
)

var XML_DESC = `
<?xml version="1.0" encoding="US-ASCII" ?>
<SB7125HF>
<Name deviceid="7125" subsystemid="1" enumerator="0">7125 (200kHz)</Name>
 <SonarType type="0" unit="nb">Bathymetric sonar</SonarType> <ArrayType type="1" unit="nb">Flat array</ArrayType> <RxElements min="0" max="127" unit="nb">Receive
ceramics.</RxElements><RxBeams min="0" max="108" unit="nb">Receive beams.</RxBeams>
<RxBeamSpacing uniform="yes" angles="0.022" unit="rad">Receiver beamspacing.</RxBeamSpacing>
<RxBeamWidth uniformacross="yes" uniformalong="yes" across="0.008726646" along="0.471238898" unit="rad">Receiver beamspacing.</RxBeamWidth>
<RxBeamPosition first="2.5" last="0.0" unit="rad">Receiver beam positions.</RxBeamPosition>
<RxBeamStabilization type="0" />
<TxBeams min="0" max="0" unit="nb">Transmit
beams.</TxBeams>
<TxBeamSteering steerable="no" maxx="0.0" minx="0.0" maxz="0.0"
minz="0.0" unit="rad">Transmit beam
steering</TxBeamSteering>
<TxBeamSpacing uniform="yes" angles="0.0" unit="rad">Transmit
beamspacing.</TxBeamSpacing>
<TxBeamWidth variable="no" maxx="0.471238898"
minx="0.471238898" maxz="2.094395102" minz="2.094395102" unit="rad">Transmit beamwidth.</TxBeamWidth>
<TxBeamStabilization type="0" />
<Frequency chirp="No" min="200000.0" max="200000.0"
unit="hz">Transmit frequency.</Frequency>
<SampleRate rate="34482.75862" unit="hz">Receiver sample
rate.</SampleRate>
<Power min="185.0" max="220.0" unit="dB//uPa">Transmit
power.</Power>
<Gain min="0.0" max="83.0" unit="dB">Receiver gain</Gain> <TxPulseLength min="0.000010" max="0.000300"
type="Rectangular" unit="s">Transmit pulse
length.</TxPulseLength>
<Range min="5.0" max="500.0" unit="m">Operating
range.</Range>
<PingRate min="0.0" max="20.0" unit="p/s">Ping rate.</PingRate>
</SB7125HF>
`

func TestXMLParse(t *testing.T) {
	cf := &Config{}
	cf.Devs = make([]Device, 1)
	cf.Hdr.Ndevs = 1
	cf.Devs[0].Info = []byte(XML_DESC + "\x00")
	di := cf.DeviceInfo()
	if len(di) != 1 {
		t.Errorf("Bad slice length: %+v", di)
	}
	if di[0].Deviceid != uint(7125) || di[0].Enumerator != uint(0) {
		t.Errorf("Parsing error: %+v", di)
	}
	if di[0].Name != "7125 (200kHz)" {
		t.Errorf("Bad name: %q", di[0].Name)
	}
	n := len(di[0].Details)
	if di[0].Details[n-1] == '\x00' {
		t.Error("Nul terminator not removed from XML")
	}
}
