package reson

import (
	"encoding/binary"
	"encoding/json"
	"io"
)

type ControlFlags uint32

func (cf ControlFlags) MarshalJSON() ([]byte, error) {
	m := make(map[string]interface{})
	x := uint32(cf)
	m["auto_range_method"] = x & 0xf
	m["auto_bd_filter_method"] = (x & 0xf0) >> 4
	m["bd_range"] = (x & 0x100) == 0x100
	m["bd_depth"] = (x & 0x200) == 0x200
	m["auto_gain_method"] = (x & 0x7c00) >> 10
	return json.Marshal(m)
}

func (cf *ControlFlags) UnmarshalJSON(b []byte) error {
	m := make(map[string]interface{})
	if err := json.Unmarshal(b, &m); err != nil {
		return err
	}
	*cf = 0
	*cf |= ControlFlags(m["auto_range_method"].(float64))
	*cf |= (ControlFlags(m["auto_bd_filter_method"].(float64)) << 4)
	if m["bd_range"].(bool) {
		*cf |= 0x100
	}
	if m["bd_depth"].(bool) {
		*cf |= 0x200
	}
	*cf |= (ControlFlags(m["auto_gain_method"].(float64)) << 10)
	return nil
}

type TransmitFlags uint32

func (tf TransmitFlags) MarshalJSON() ([]byte, error) {
	m := make(map[string]interface{})
	x := uint32(tf)
	m["pitch_stabilization"] = x & 0xf
	m["yaw_stabilization"] = (x & 0xf0) >> 4
	return json.Marshal(m)
}

func (tf *TransmitFlags) UnmarshalJSON(b []byte) error {
	m := make(map[string]interface{})
	if err := json.Unmarshal(b, &m); err != nil {
		return err
	}
	*tf = 0
	*tf |= (TransmitFlags(m["pitch_stabilization"].(float64)) & 0xf)
	*tf |= (TransmitFlags(m["yaw_stabilization"].(float64)) << 4)
	return nil
}

type ProjectorInfo struct {
	Id           uint32  `json:"id"`
	Vert_angle   float32 `json:"vert_angle"`
	Horiz_angle  float32 `json:"horiz_angle"`
	Vert_width   float32 `json:"vert_width"`
	Horiz_width  float32 `json:"horiz_width"`
	Focal_point  float32 `json:"focal_point"`
	Window_type  uint32  `json:"window_type"`
	Window_param float32 `json:"window_param"`
}

type ReceiverInfo struct {
	Window_type  uint32  `json:"window_type"`
	Window_param float32 `json:"window_param"`
	Flags        uint32  `json:"flags"`
	Beam_width   float32 `json:"beam_width"`
}

type BottomDetect struct {
	Min_range float32 `json:"min_range"`
	Max_range float32 `json:"max_range"`
	Min_depth float32 `json:"min_depth"`
	Max_depth float32 `json:"max_depth"`
}

// Reson 7K Settings data record, type 7000.
type SettingsHeader struct {
	Sonar_id       uint64        `json:"sonar_id"`
	Ping_num       uint32        `json:"ping_num"`
	Multi_ping     uint16        `json:"multi_ping"`
	Xmit_freq      float32       `json:"xmit_freq"`
	Sample_rate    float32       `json:"sample_rate"`
	Rcvr_bandwidth float32       `json:"rcvr_bandwidth"`
	Pulse_width    float32       `json:"pulse_width"`
	Pulse_type     uint32        `json:"pulse_type"`
	Envelope_type  uint32        `json:"envelope_type"`
	Envelope_param float32       `json:"envelope_param"`
	Pulse_extra    uint32        `json:"pulse_extra"`
	Max_ping_rate  float32       `json:"max_ping_rate"`
	Ping_period    float32       `json:"ping_period"`
	Range_sel      float32       `json:"range_sel"`
	Power_sel      float32       `json:"power_sel"`
	Gain_sel       float32       `json:"gain_sel"`
	Control        ControlFlags  `json:"control"`
	Prj            ProjectorInfo `json:"prj"`
	Transmit       TransmitFlags `json:"transmit"`
	Hydrophone_id  uint32        `json:"hydrophone_id"`
	Recv           ReceiverInfo  `json:"recv"`
	Bd             BottomDetect  `json:"bd"`
	Absorption     float32       `json:"absorption"`
	Sound_speed    float32       `json:"sound_speed"`
	Spreading_loss float32       `json:"spreading_loss"`
	_              uint16
}

type Settings struct {
	Hdr SettingsHeader `json:"hdr"`
}

func init() {
	drRegistry[uint32(7000)] = func() DataRecord { return &Settings{} }
}

// Unpack implements the reson.DataRecord interface
func (s *Settings) Unpack(rdr io.Reader) error {
	return binary.Read(rdr, ByteOrder, &s.Hdr)
}

// ReadSettings returns a new Settings struct from a data source.
func ReadSettings(rdr io.Reader) (*Settings, error) {
	rf := &Settings{}
	err := rf.Unpack(rdr)
	return rf, err
}
