package reson

import (
	"bytes"
	"encoding/binary"
	"encoding/xml"
	"io"
	"strconv"
	"strings"
)

type DeviceHeader struct {
	// Device ID number
	Id uint32 `json:"id"`
	// Device description
	Desc [64]byte `json:"desc"`
	// Serial number
	Serial_num uint64 `json:"serial_num"`
	// Length of the device info in bytes
	Info_len uint32 `json:"info_len"`
}

type Device struct {
	Hdr  DeviceHeader `json:"hdr"`
	Info []byte       `json:"info"`
}

// Reson 7K Configuration record, type 7001
type ConfigHeader struct {
	Sonar_id uint64 `json:"sonar_id"`
	Ndevs    uint32 `json:"ndevs"`
}

type Config struct {
	Hdr  ConfigHeader `json:"hdr"`
	Devs []Device     `json:"devices"`
}

func init() {
	drRegistry[uint32(7001)] = func() DataRecord { return &Config{} }
}

// Information returned by the DeviceInfo method
type DevInfo struct {
	// Device type ID
	Deviceid uint
	// Device enumerator
	Enumerator uint
	// Device name
	Name string
	// XML encoded device description
	Details string
}

// Top level element in the XML device info
type sb7125 struct {
	XMLName xml.Name
	//Info    DevInfo  `xml:"Name"`
	Info struct {
		Deviceid   string `xml:"deviceid,attr"`
		Enumerator string `xml:"enumerator,attr"`
		Name       string `xml:",chardata"`
	} `xml:"Name"`
}

// Unpack implements the reson.DataRecord interface
func (cf *Config) Unpack(rdr io.Reader) error {
	err := binary.Read(rdr, ByteOrder, &cf.Hdr)
	if err == nil {
		cf.Devs = make([]Device, cf.Hdr.Ndevs)
		for i := 0; i < int(cf.Hdr.Ndevs); i++ {
			err = binary.Read(rdr, ByteOrder, &cf.Devs[i].Hdr)
			if err == nil {
				cf.Devs[i].Info = make([]byte, cf.Devs[i].Hdr.Info_len)
				_, err = io.ReadFull(rdr, cf.Devs[i].Info)
				if err != nil {
					break
				}
			}
		}
	}
	return err
}

// ReadConfig reads a Config packet from a data source.
func ReadConfig(rdr io.Reader) (*Config, error) {
	cf := &Config{}
	err := cf.Unpack(rdr)
	return cf, err
}

// DeviceInfo returns a list of device IDs and enumerators
func (cf *Config) DeviceInfo() []DevInfo {
	di := make([]DevInfo, 0)
	v := sb7125{}
	for _, dev := range cf.Devs {
		dec := xml.NewDecoder(bytes.NewReader(dev.Info))
		// The XML is US-ASCII encoded which is a subset of UTF-8
		dec.CharsetReader = func(charset string, input io.Reader) (io.Reader, error) {
			return input, nil
		}
		err := dec.Decode(&v)
		if err != nil {
			break
		}
		dev_id, _ := strconv.ParseUint(v.Info.Deviceid, 10, 32)
		dev_enum, _ := strconv.ParseUint(v.Info.Enumerator, 10, 16)
		di = append(di,
			DevInfo{
				Deviceid:   uint(dev_id),
				Enumerator: uint(dev_enum),
				Name:       v.Info.Name,
				Details:    strings.TrimRight(string(dev.Info), "\x00")})
	}
	return di
}
