package reson

import (
	"bytes"
	"encoding/binary"
	"encoding/json"
	"io"
)

// Reson 7K BITE record, type 7021
type BITEData struct {
	N      uint16      `json:"n"`
	Boards []BITEBoard `json:"boards"`
}

// Nul-padded ASCII string
type ZString [64]byte

func (zs ZString) MarshalJSON() ([]byte, error) {
	b := bytes.TrimRight(zs[:], "\x00")
	return json.Marshal(string(b))
}

type BoardHeader struct {
	SrcName    ZString   `json:"src_name"`
	SrcAddr    uint8     `json:"src_addr"`
	Frequency  float32   `json:"frequency"`
	Enumerator uint16    `json:"enumerator"`
	Tsent      Timestamp `json:"t_sent"`
	Trecv      Timestamp `json:"t_recv"`
	BITETime   Timestamp `json:"t_bite"`
	Status     uint8     `json:"status"`
	Nbf        uint16    `json:"nbf"`
	BITEStatus [4]uint64 `json:"bite_status"`
}

type BITEBoard struct {
	BoardHeader
	Devices []BITEEntry `json:"devices"`
}

type BITEDevType uint8

const (
	ErrorCount BITEDevType = iota + 1
	FPGADieTemperature
	Humidity
	SerialAdc
	FirmwareVersion
)

type BITEEntry struct {
	_       uint16
	Name    ZString     `json:"name"`
	Type    BITEDevType `json:"type"`
	Minimum float32     `json:"min"`
	Maximum float32     `json:"max"`
	Value   float32     `json:"value"`
}

func init() {
	drRegistry[uint32(7021)] = func() DataRecord { return &BITEData{} }
}

// Unpack implements the reson.DataRecord interface
func (bd *BITEData) Unpack(rdr io.Reader) error {
	err := binary.Read(rdr, ByteOrder, &bd.N)
	if err == nil {
		bd.Boards = make([]BITEBoard, bd.N)
		for i := 0; i < int(bd.N); i++ {
			err = binary.Read(rdr, ByteOrder, &bd.Boards[i].BoardHeader)
			if err != nil {
				return err
			}
			bd.Boards[i].Devices = make([]BITEEntry, bd.Boards[i].Nbf)
			for j := 0; j < int(bd.Boards[i].Nbf); j++ {
				err = binary.Read(rdr, ByteOrder, &bd.Boards[i].Devices[j])
				if err != nil {
					return err
				}
			}
		}
	}

	return nil
}

// ReadBITEData reads a BITEData packet from a data source.
func ReadBITEData(rdr io.Reader) (*BITEData, error) {
	bd := &BITEData{}
	err := bd.Unpack(rdr)
	return bd, err
}
